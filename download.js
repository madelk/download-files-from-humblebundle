clear()
var fileType = '.pdf'
var size = 30;

var arrays = []
var urlsFound = []

var nodes = $('.js-start-download a');
console.log('found ' + nodes.length + ' urls')

for (i = 0; i < nodes.length; i++) {
    var a = $(nodes[i]);
    var url = a.attr('href');
    if (!url || url.length < 5 || (url.indexOf(fileType) < 0)) continue;
    urlsFound.push(url);
}

console.log('but only ' + urlsFound.length + ' left after filtering')

while (urlsFound.length > 0)
    arrays.push(urlsFound.splice(0, size))
    
for (i = 0; i < arrays.length; i++)
    console.log(arrays[i].join('\n'))
